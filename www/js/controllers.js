angular.module('starter.controllers', [])
.controller('MapCtrl', function($scope, $ionicLoading, $ionicPopup){
    var panama = {
        lat: 8.9696043,
        lng: -79.53100899999998
    }

    var mercado = {
        lat: 8.9588162,
        lng: -79.5376018
    }

    var arraijan = {
        lat: 8.9417522,
        lng: -79.64255889999998,
    }

    var sanMiguelito = {
        lat: 9.0707553,
        lng: -79.48163779999999
    }

    var myUbication = {}

    var toggleMarkerStatus = false;
    var theListener;

    var markerCount = 1;

    initMap = function() {
        var mapDiv = document.getElementById('map');

        var mapOptions = {
            center: panama,
            zoom: 14,
            mapTypeControlOptions: {
                style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
            }
        }

        $scope.map = new google.maps.Map(mapDiv, mapOptions);

        addMarker(panama);
        addMarker(arraijan);
        addMarker(sanMiguelito);

        searchPlaces();
        //$scope.locateMe();
    }

    $scope.locateMe = function() {
        $ionicLoading.show({});

        navigator.geolocation.getCurrentPosition(function(pos) {
            myUbication.lat = pos.coords.latitude;
            myUbication.lng = pos.coords.longitude;

            $scope.map.setCenter(myUbication);

            $ionicLoading.hide();
            addMarker(myUbication);

        }, function(error) {
            $ionicLoading.hide();
        })
    }

    addMarker = function(ubication) {
        var marker = new google.maps.Marker({
            map: $scope.map,
            position: ubication,
            draggable: true,
            label: String(markerCount),
            animation: google.maps.Animation.DROP
        })

        markerCount++;
        marker.addListener('dblclick', deleteMarker)
        marker.addListener('click', addInfoWindow)
    }

    searchPlaces = function() {
        var searchInput = document.getElementById('searchInput');
        var searchBox   = new google.maps.places.SearchBox(searchInput);
        $scope.map.controls[google.maps.ControlPosition.TOP_LEFT].push(searchInput);

        searchBox.addListener('places_changed', function(){
            var places = searchBox.getPlaces();

            places.forEach(function(item){
                var ubication = item.geometry.location;
                addMarker(ubication);
                $scope.map.setCenter(ubication);
                traceRoute(ubication);
            })
        })
    }

    traceRoute = function(ubication) {
        var directionsDisplay = new google.maps.DirectionsRenderer({
            map: $scope.map
        })

        var request = {
            destination: ubication,
            origin: myUbication,
            travelMode: google.maps.TravelMode.WALKING
        }

        var directionsService = new google.maps.DirectionsService();

        directionsService.route(request, function(response, status){
            if(status == google.maps.DirectionsStatus.OK) {
                directionsDisplay.setDirections(response);
            }
        })
    }

    $scope.toggleAddMarker = function() {
        if(toggleMarkerStatus){
            toggleMarkerStatus = false;
            google.maps.event.removeListener(theListener);
        }
        else{
            toggleMarkerStatus = true;
            theListener = $scope.map.addListener('click', addMarkerAtMouse);
        }
    }

    addMarkerAtMouse = function(event) {
        addMarker(event.latLng)
    }

    deleteMarker = function() {
        this.setMap(null);
    }

    addInfoWindow = function(event) {
        var geocoder    = new google.maps.Geocoder;
        var infoWindow  = new google.maps.InfoWindow({
            maxWidth: 100
        });
        var self        = this;

        geocoder.geocode({'location': event.latLng}, function(results, status) {
            if(status === google.maps.GeocoderStatus.OK) {
                console.log(results);
                var placeName       = results[1].address_components[0].short_name;
                var placeAddress    = results[1].formatted_address;
                var infoContent     = `${placeName}<hr>${placeAddress}`;

                infoWindow.setContent(infoContent);
                infoWindow.open($scope.map, self);
            }
        })
    }

    $scope.paintRoute = function() {
        var routeCoords = [panama, sanMiguelito, arraijan];
        var routePath   = new google.maps.Polygon({
            path: routeCoords,
            strokeColor: '#0066ff',
            strokeOpacity: 1.0,
            strokeWeight: 2,
            editable: true
        })

        routePath.setMap($scope.map);
    }

    var layer;

    $scope.clearLayer = function() {
        if( typeof layer !== 'undefined' ) {
            layer.setMap(null);
        }
    }

    $scope.showTraffic = function() {
        $scope.clearLayer();
        layer = new google.maps.TrafficLayer();
        layer.setMap($scope.map);
    }

    $scope.showTransit = function() {
        $scope.clearLayer();
        layer = new google.maps.TransitLayer();
        layer.setMap($scope.map);
    }

    if(document.readyState === 'complete') {
        initMap();
    } else {
        google.maps.event.addDomListener(window, 'load', initMap);
    }
})
